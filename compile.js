const path = require('path');
const fs = require('fs');
const solc = require('solc');

const signPath = path.resolve(__dirname, 'contracts', 'ethereumsign.sol');
const source = fs.readFileSync(signPath, 'utf8');
module.exports = solc.compile(source, 1).contracts[':EthereumSign'];
// var output = solc.compile(source, 1);
// for (var contractName in output.contracts) { 
//     // code and ABI that are needed by web3 
//     console.log(contractName + ': ' + output.contracts[contractName].bytecode);
//     console.log(contractName + '; ' + JSON.parse(output.contracts[contractName].interface));
//  }