import web3 from './web3';


const address = '0x6fa6D2a024182fC792F42f5B1cbEC106d0c7b6b0';
const abi = [{"constant":false,"inputs":[{"name":"guid","type":"string"},{"name":"hash","type":"string"}],"name":"Sign","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"guid","type":"string"},{"name":"signer","type":"address"}],"name":"GetSign","outputs":[{"name":"sign","type":"bytes32"},{"name":"signedVersion","type":"uint8"},{"name":"timestamp","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"x","type":"address"}],"name":"toString","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"x","type":"bytes32"}],"name":"bytes32ToString","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"guid","type":"string"},{"name":"version","type":"uint8"},{"name":"signature","type":"bytes32"}],"name":"Check","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"guid","type":"string"},{"name":"signer","type":"address"},{"name":"version","type":"uint256"}],"name":"GetSignVersion","outputs":[{"name":"sign","type":"bytes32"},{"name":"signedVersion","type":"uint8"},{"name":"timestamp","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]
export default new web3.eth.Contract(abi, address);


