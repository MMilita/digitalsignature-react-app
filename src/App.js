import React, { Component } from 'react';
import web3 from './web3';
import ethereumsign from './ethereumsign';
import './App.css';


const Card = {
  padding: "24px",
  borderRadius: "0 0 2px 2px",
  position: "relative",
  margin: ".5rem 0 1rem 0",
  backgroundColor: "#fff",
  transition: "box-shadow .25s, -webkit-box-shadow .25s",
  borderRadius: "2px"
}

class App extends Component {
  state = {
    hash : '',
    guid : '',
    signature : '',
    version : '',
    message : '',
    timestamp : ''
  };

  // async componentDidMount() {
  //   // const sign = await ethereumsign.methods.Sign().send();
  //   // const getSign = await ethereumsign.methods.GetSign().call();
  //   // const check = await ethereumsign.methods.Check().call();
  //   // //balance is not a number but an object, instantiate it in state with empty string
  //   // // const balance = await web3.eth.getBalance(lottery.options.address);
    
  //   // // this.setState({manager, players, balance});
  // }
    
  onSubmit = async (event) => {

      event.preventDefault();
      
      // const accounts = await web3.eth.getAccounts();
       const accounts = '0x30eaa0d8cba644f08f0e200ac60d4b6d9d7c9192';
       const gasLimit = '4700000';
      console.log(accounts);
      
      this.setState({message: 'Waiting transaction success...'});
      
      const guid = this.state.guid;
      const hash = this.state.hash;

      await ethereumsign.methods.Sign(guid, hash).send({
        from: accounts,
        gas : gasLimit
      });

      const success = await ethereumsign.methods.GetSign(guid, accounts).call();
      console.log(success)

      this.setState({
        message: 'Signature complete',
        version: success.signedVersion,
        signature: success.sign,
        timestamp : success.timestamp
        
                    
    });
    };

    onClick = async () => {

      // const accounts = await web3.eth.getAccounts();
      const accounts = '0x30eaa0d8cba644f08f0e200ac60d4b6d9d7c9192';
      
      this.setState({message: 'waiting transaction success'});
      const check = await 
      web3.eth.call({ 
        from : accounts,
        to: '0x6fa6D2a024182fC792F42f5B1cbEC106d0c7b6b0', 
        data: ethereumsign.methods.Check(this.state.guid,  this.state.version, this.state.signature).encodeABI() 
      }).then(data => {
        console.log(data)});
      
      // const check = await ethereumsign.methods.Check("aa", 1, '0xc89efdaa54c0f20c7adf612882df0950f5a951637e0307cdcb4c672f298b8bc6').call();
      // const check = await ethereumsign.methods.Check(this.state.guid, this.state.version, this.state.signature).call();
      // console.log(check);
    //   this.setState({message: ''});
    };




  render() {
    return (
      <div style={Card}>
      <h1>My Contract</h1>
        <form onSubmit={this.onSubmit}>
        <label>Identifier GUID</label>
        <input value={this.state.guid} onChange={event => this.setState({guid: event.target.value})}/>
        <label>hash</label>
        <input value={this.state.hash} onChange={event => this.setState({hash: event.target.value})}/>
        <button>Submit</button>
        </form>
        <hr/>
        <h3>Signature: {this.state.signature}</h3>
        <h3>Version: {this.state.version}</h3>
        <h3>Timestamp: {this.state.timestamp}</h3>
        <hr/>
        <button onClick={this.onClick}>Check Signature</button>
        <hr/>
        <h3>{this.state.message}</h3>
    </div> 
     
      );
  }
} 
  

export default App;
